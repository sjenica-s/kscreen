# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-05 00:47+0000\n"
"PO-Revision-Date: 2019-10-31 00:08+0200\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.1\n"

#: daemon.cpp:129
msgid "Switch Display"
msgstr "Vaheta monitori"

#~ msgid "Switch to external screen"
#~ msgstr "Lülitu välisele ekraanile"

#~ msgid "Switch to laptop screen"
#~ msgstr "Lülitu sülearvuti ekraanile"

#~ msgid "Unify outputs"
#~ msgstr "Ühenda väljundid"

#~ msgid "Extend to left"
#~ msgstr "Laienda vasakule"

#~ msgid "Extend to right"
#~ msgstr "Laienda paremale"

#~ msgid "Leave unchanged"
#~ msgstr "Ei muudeta"

#~ msgctxt "OSD text after XF86Display button press"
#~ msgid "No External Display"
#~ msgstr "Väline kuva puudub"

#~ msgctxt "OSD text after XF86Display button press"
#~ msgid "Changing Screen Layout"
#~ msgstr "Ekraanipaigutuse muutmine"

#, fuzzy
#~| msgctxt "OSD text after XF86Display button press"
#~| msgid "No External Display"
#~ msgctxt "osd when displaybutton is pressed"
#~ msgid "Cloned Display"
#~ msgstr "Väline kuva puudub"

#, fuzzy
#~| msgctxt "OSD text after XF86Display button press"
#~| msgid "No External Display"
#~ msgctxt "osd when displaybutton is pressed"
#~ msgid "External Only"
#~ msgstr "Väline kuva puudub"
